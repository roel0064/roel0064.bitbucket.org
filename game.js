var won = 0;
var tie = 0;
var lost = 0;

function compare(choice) {
    var choice2 = Math.random();
    if (choice2 < 0.34) {
        choice2 = "rock";
    } else if (choice2 <= 0.67) {
        choice2 = "paper";
    } else {
        choice2 = "scissors";
    }
    if (choice === choice2) {
        tie += 1;
        uitslag();
        alert("Computer chose the same: It's a tie")
    } else if (choice === "rock") {
        if (choice2 === "scissors") {
            won += 1;
            uitslag();
            alert("Computer chose scissors: You won")
        } else {
            lost += 1;
            uitslag();
            alert("Computer chose paper: You lost")
        }
    } else if (choice === "paper") {
        if (choice2 === "rock") {
            won += 1;
            uitslag();
            alert("Computer chose rock: You won")
        } else {
            lost += 1;
            uitslag();
            alert("Computer chose scissors: You lost")
        }
    } else if (choice === "scissors") {
        if (choice2 === "paper") {
            won += 1;
            uitslag();
            alert("Computer chose paper: You won")
        } else {
            lost += 1;
            uitslag();
            alert("Computer chose stone: You lost")
        }
    }
};

function uitslag() {
    document.getElementById("tie").innerHTML = tie;
    document.getElementById("won").innerHTML = won;
    document.getElementById("lost").innerHTML = lost;
};
